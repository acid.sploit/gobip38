package bip38

import (
	"testing"
	"github.com/gcash/bchutil"
)

var testNonECM = []struct {
		label string
		pass string
		wif string
		secret string
	}{
		{
			label: "uncompressed",
			pass : "TestingOneTwoThree",
			wif : "5KN7MzqK5wt2TP1fQCYyHBtDrXdJuXbUzm4A9rKAteGu3Qi5CVR",
			secret : "6PRVWUbkzzsbcVac2qwfssoUJAN1Xhrg6bNk8J7Nzm5H7kxEbn2Nh2ZoGg",
		},
		{
			label: "uncompressed",
			pass: "Satoshi",
			wif: "5HtasZ6ofTHP6HCwTqTkLDuLQisYPah7aUnSKfC7h4hMUVw2gi5",
			secret: "6PRNFFkZc2NZ6dJqFfhRoFNMR9Lnyj7dYGrzdgXXVMXcxoKTePPX1dWByq",
		},
		{
			label: "compressed",
			pass: "TestingOneTwoThree",
			wif: "L44B5gGEpqEDRS9vVPz7QT35jcBG2r3CZwSwQ4fCewXAhAhqGVpP",
			secret: "6PYNKZ1EAgYgmQfmNVamxyXVWHzK5s6DGhwP4J5o44cvXdoY7sRzhtpUeo",
		},
		{
			label: "compressed",
			pass: "Satoshi",
			wif: "KwYgW8gcxj1JWJXhPSu4Fqwzfhp5Yfi42mdYmMa4XqK7NJxXUSK7",
			secret: "6PYLtMnXvfG3oJde97zRyLYFZCYizPU5T3LwgdYJz1fRhh16bU7u6PPmY7",
		},
	}

func TestEncrypt(t *testing.T) {
	for _, v := range testNonECM {
		wif, _ := bchutil.DecodeWIF(v.wif)
		encrypt, err := Encrypt(wif, v.pass)
		if err != nil {
			t.Fatal(err)
		}
		if (encrypt != v.secret) {
			t.Fatal(v.label)
		}
	}
}

func TestDecrypt(t *testing.T) {

	for _, v := range testNonECM {
		wif, err := Decrypt(v.secret, v.pass)
		if err != nil {
			t.Fatal(err)
		}
		if (wif.String() != v.wif) {
			t.Fatal(v.label)
		}
	}
}

var testECMDecrypt = []struct {
		label string
		pass string
		wif string
		secret string
	}{
		{
			label: "compressed",
			pass: "test1234",
			wif: "L1ZL7jXg3hBW6ASzJzfsAhMR6nfpFZXoEaCRKdaaXV51AGpDLX8i",
			secret: "6PnVosrWA2sXPLowM33jc2p57NT3DheM9DP8y5ERkBrE2h3CZLgfajzn6U",
		},
		{
			label: "compressed",
			pass: "Satoshi",
			wif: "KwNeYnuMpNQgY4ntxjEXbZCE3ddRpwSsiJLXm2m5dHSDQeL8ST3C",
			secret: "6PoELtW9c366a5pXEtDvXgn7NxuFgxRDGiXMH2wyM6MLSc7pf7cAHz7TcR",
		},
	}

func TestDecryptECM(t *testing.T) {

	for _, v := range testECMDecrypt {
		wif, err := Decrypt(v.secret, v.pass)
		if err != nil {
			t.Fatal(err)
		}
		if (wif.String() != v.wif) {
			t.Fatal(v.label)
		}
	}
}


// var testECMEncrypt = []struct {
// 		label string
// 		pass string
// 		intpass string
// 		wif string
// 		secret string
// 		addr string
// 	}{
// 		{
// 			label: "ECMcompressed",
// 			pass: "test1234",
// 			wif: "",
// 			secret: "",
// 			addr: "",
// 		},
// 		{
// 			label: "ECMcompressed",
// 			pass: "Satoshi",
// 			wif: "",
// 			secret: "",
// 			addr: "",
// 		},
// 	}

// func TestECMEncrypt(t *testing.T) {
// 	for _, v := range testECMEncrypt {
// 		v.secret, v.addr, err := EncryptECMultiply(v.intpass)
// 		if err != nil {
// 			t.Fatal(err)
// 		}
// 		if (secret != v.secret) {
// 			t.Fatal(v.label, secret, v.secret)
// 		}
// 		if (addr != v.addr) {
// 			t.Fatal(v.label, addr, v.addr)
// 		}
// 	}
// }