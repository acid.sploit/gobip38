# gitlab.com/acid.sploit/gobip38/bip38

```
go get gitlab.com/acid.sploit/gobip38/bip38
```

This repository contains a Golang BIP38 package exporting all functions needed to encrypt WIF keys, decrypt and generate new BIP38 keys, complying with the BIP38 spec.

- Encrypt WIF key (Compressed & Uncompressed)
- Generate new WIF key and BIP38 encrypt with password
- Generate new intermediary passphrase (EC Multiplied)
- Generate new BIP38 key from intermediary passphrase (EC Multiplied)
- Decrypt BIP38 secret (Non EC Multiplied & EC Multiplied) (Compressed & Uncompressed)


```
// Encrypt WIF with password
func Encrypt(wif *bchutil.WIF, password string) (string, error) 

// Generate intermediate passphrase to generate EC Multiplied keys
func GeneratePassphrase(password string, salt []byte) (string, error)

// Generate EC Multiplied BIP38 key (generate BIP38 key without knowing the private key)
func EncryptECMultiply(intermediatePassphrase string) (string, string, error)

// Catchall Decrypt function - automatically detect ECM or NonECM
func Decrypt(encryptedKey string, password string) (*bchutil.WIF, error)

// Decrypt BIP38 keys where EC Multiplication is not used
func DecryptNonECMultiplied(b []byte, password string) (*bchutil.WIF, error)

// Decrypt BIP38 keys where EC Multiplication is used
func DecryptECMuliplied(b []byte, passphrase string) (*bchutil.WIF, error)

// Return full SHA256(SHA256()) hash
func Hash256(buf []byte) []byte

// Return last 4 bytes of a SHA256(SHA256()) hash
func CheckSum(buf []byte) []byte
```

## Example code

### Decrypt()
```
func decryptBip38(bip38String *string, password *string) (*bchutil.WIF, error) {
	b := base58.Decode(*bip38String)
	wif, err := bip38.Decrypt(*bip38String, *password)
	if err != nil {
		return nil, err
	}

	return wif, nil
}

```

### DecryptNonECMultiplied()
```
func decryptBip38NonECM(bip38String *string, password *string) (*bchutil.WIF, error) {
	byteKey := base58.Decode(*bip38String)
	wif, err := DecryptNonECMultiplied(byteKey, password)
	if err != nil {
		return nil, err
	}

	return wif, nil
}

```

### Encrypt()
```
func encryptWif(wifString *string, password *string) (string, error) {
	wif, err := bchutil.DecodeWIF(*wifString)
	if err != nil {
		return "", err
	}

	encryptWif, err := bip38.Encrypt(wif, *password)
	if err != nil {
		return "", err
	}

	return encryptWif, nil
}
```

### GeneratePassphrase()
```
salt := make([]byte, 4)
rand.Read(salt)

intermediatePassphraseString, _ := bip38.GeneratePassphrase(*passPtr, salt)
```

### EncryptECMultiply()
```
bip38, addr, err := bip38.EncryptECMultiply(intermediatePassphraseString)
if err != nil {
	log.Fatal(err)
	return
}
```