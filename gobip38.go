package main

import (
	"fmt"
	"log"
	"flag"
	"syscall"
	"crypto/rand"
	"encoding/hex"
	"gitlab.com/acid.sploit/gobip38/bip38"
	"github.com/gcash/bchutil"
	"github.com/gcash/bchutil/base58"
	"github.com/gcash/bchd/chaincfg"
	"github.com/gcash/bchd/bchec"
	"golang.org/x/crypto/ssh/terminal"
	"github.com/schancel/cashaddr-converter/address"
	)

func main() {
	wifPtr := flag.String("w", "", "Base58 WIF key")
	bip38Ptr := flag.String("b", "", "Base58 BIP38 encrypted key")
	passPtr := flag.String("p", "", "Password (The application will ask for the password when omitted)")
	newPtr := flag.Bool("n", false, "Generate new BIP38 encryped WIF key and address pair.")
	newECMPtr := flag.Bool("N", false, "Generate new EC Multiplied BIP38 encryped WIF key and address pair.")

	flag.Parse()

	if *wifPtr == "" && *bip38Ptr == "" && *newPtr == false && *newECMPtr == false {
		flag.Usage()
		return
	}

	if *wifPtr != "" && *bip38Ptr != "" {
		log.Fatal("Error: -w and -b are mutually exclusive!")
		return
	}

	if *passPtr == "" {
		fmt.Println("Enter your password: ")
		bytePassword, _ := terminal.ReadPassword(int(syscall.Stdin))
		password := string(bytePassword)
		*passPtr = password
	}


	if *newPtr == true {
		if *wifPtr != "" || *bip38Ptr != "" {
			log.Fatal("Error: -n can not be used in combination with -b or -w.")
			return
		}

		privateKey, err := bchec.NewPrivateKey(bchec.S256())
		if err != nil {
			log.Fatal(err)
			return
		}

		wif, err := bchutil.NewWIF(privateKey, &chaincfg.MainNetParams, true)
		if err != nil {
			log.Fatal(err)
			return
		}

		pub, err := bchutil.NewAddressPubKey(wif.SerializePubKey(), &chaincfg.MainNetParams)
		if err != nil {
			log.Fatal(err)
			return
		}

		rawAddress, _ := address.NewFromString(pub.EncodeAddress())
		cashAddress, err := rawAddress.CashAddress()
		if err != nil {
			log.Fatal(err)
			return
		}
		caddr, err := cashAddress.Encode()
		if err != nil {
			log.Fatal(err)
			return
		}

		bip38, err := encryptWif(wif.String(), passPtr)

		//fmt.Printf("%10s %s\n", "WIF:", wif.String())
		fmt.Printf("%10s %s\n", "LEGACY:", pub.EncodeAddress())
		fmt.Printf("%10s %s\n", "CASHADDR:", caddr)	
		fmt.Printf("%10s %s\n", "BIP38:", bip38)

		return
	}

	// https://github.com/bitcoin/bips/blob/master/bip-0038.mediawiki#encryption-when-ec-multiply-mode-is-used
	if *newECMPtr == true {
		if *wifPtr != "" || *bip38Ptr != "" {
			log.Fatal("Error: -n can not be used in combination with -b or -w.")
			return
		}

		// BIP38 Owner 

		// 1. Generate 4 random bytes, call them ownersalt.
		salt := make([]byte, 4)
		rand.Read(salt)

		intermediatePassphraseString, _ := bip38.GeneratePassphrase(*passPtr, salt)

		// BIP38 Printer
		bip38, addr, err := bip38.EncryptECMultiply(intermediatePassphraseString)
		if err != nil {
			log.Fatal(err)
			return
		}

		rawAddress, _ := address.NewFromString(addr)
		cashAddress, err := rawAddress.CashAddress()
		if err != nil {
			log.Fatal(err)
			return
		}
		caddr, err := cashAddress.Encode()
		if err != nil {
			log.Fatal(err)
			return
		}

		fmt.Println("NEW EC-Multiply Type BIP38 Key & Associated Address:")
		fmt.Printf("%10s %s\n", "LEGACY:", addr)
		fmt.Printf("%10s %s\n", "CASHADDR:", caddr)
		fmt.Printf("%10s %s\n", "BIP38:", bip38)
		
		return
	}

	if *wifPtr != "" {
		fmt.Println("INPUT")
		fmt.Printf("%10s %s\n", "WIF:", *wifPtr)
		fmt.Printf("%10s %s\n", "Password:", *passPtr)

		bip38, err := encryptWif(*wifPtr, passPtr)
		if err != nil {
			log.Fatal(err)
			return
		}

		fmt.Printf("\n%s\n", "BIP38 ENCRYPTED WIF")
		fmt.Printf("%10s %s\n", "BIP38:", bip38)

		return
	}

	if *bip38Ptr != "" {
		fmt.Println("INPUT")
		fmt.Printf("%10s %s\n", "BIP38:", *bip38Ptr)
		fmt.Printf("%10s %s\n", "Password:", *passPtr)

		wif, err := decryptBip38(bip38Ptr, passPtr)
		if err != nil {
			log.Fatal(err)
			return
		}

		pub, err := bchutil.NewAddressPubKey(wif.SerializePubKey(), &chaincfg.MainNetParams)
		if err != nil {
			log.Fatal(err)
			return
		}

		rawAddress, _ := address.NewFromString(pub.EncodeAddress())
		cashAddress, err := rawAddress.CashAddress()
		if err != nil {
			log.Fatal(err)
			return
		}
		addr, err := cashAddress.Encode()
		if err != nil {
			log.Fatal(err)
			return
		}

		fmt.Printf("\n%s\n","DECRYPTED WIF")
		fmt.Printf("%10s %s\n", "WIF:", wif.String())
		fmt.Printf("%10s %s\n", "LEGACY:", pub.EncodeAddress())
		fmt.Printf("%10s %s\n", "CASHADDR:", addr)	

		return
	}
}

func decryptBip38(bip38String *string, password *string) (*bchutil.WIF, error) {
	b := base58.Decode(*bip38String)
	fmt.Printf("%10s %s\n", "HEX KEY:", hex.EncodeToString(b))

	wif, err := bip38.Decrypt(*bip38String, *password)
	if err != nil {
		return nil, err
	}


	return wif, nil
}

func encryptWif(wifString string, password *string) (string, error) {
	wif, err := bchutil.DecodeWIF(wifString)
	if err != nil {
		return "", err
	}

	encryptWif, err := bip38.Encrypt(wif, *password)
	if err != nil {
		return "", err
	}

	return encryptWif, nil
}
