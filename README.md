# GOBIP38
A Golang BIP38 package/library and a CLI tool implementing it. 

The gobip38 tool can generate new EC Multiplied BIP38 keys from password, encrypts existing WIF keys and generate new WIF keys. As well as decrypt all types of BIP38 keys.

## Run (Linux)
Run the shipped binary ./gobip38 from the root of this git project.

## Develop/Build

### Dependencies

```
go get gitlab.com/acid.sploit/gobip38/bip38
```

### Build
```
go build gobip38.go
```

### Run
```
./gobip38 -h
```

### Go run
```
go run gobip38.go -h
```


## Usage
```
Usage of ./gobip38:
  -N    Generate new EC Multiplied BIP38 encryped WIF key and address pair.
  -b string
        Base58 BIP38 encrypted key
  -n    Generate new BIP38 encryped WIF key and address pair.
  -p string
        Password (The application will ask for the password when omitted)
  -w string
        Base58 WIF key


# Generate new BIP38 key from password
./gobip38 -p pass1234 -N

NEW EC-Multiply Type BIP38 Key & Associated Address:
   LEGACY: 1NJXcSpCcoDLy4wzkGrvSa3L5QGDejjnha
 CASHADDR: bitcoincash:qr56k3qyzuyzwryrceej6atnhz95cgk52qupe27j5u
    BIP38: 6PoPpGt4ib4cbt8kLM5M5BNY2Ao4qqsWs2bTQQE55wWynGNxEKfJ5VHY7V


# Generate new WIF key and encrypt with password
./gobip38 -p pass1234 -n

   LEGACY: 1JX4ggCdNtmqi89Cq1ksjQq2Ej8vwASAm2
 CASHADDR: bitcoincash:qrqznv2x0m5lakekr0272yg7r5xc4ttfc50j4a97kp
    BIP38: 6PYQgvb7NvgHKbEp1k74o1dCEgS682fjMBYkuALmtAXQnBXuT4kGSbaRqg


# Encrypt WIF
./gobip38 -w KwYgW8gcxj1JWJXhPSu4Fqwzfhp5Yfi42mdYmMa4XqK7NJxXUSK7 -p Satoshi

INPUT
      WIF: KwYgW8gcxj1JWJXhPSu4Fqwzfhp5Yfi42mdYmMa4XqK7NJxXUSK7
 Password: Satoshi

BIP38 ENCRYPTED WIF
    BIP38: 6PYLtMnXvfG3oJde97zRyLYFZCYizPU5T3LwgdYJz1fRhh16bU7u6PPmY7


# Decrypt BIP38 key
./gobip38 -b 6PYLtMnXvfG3oJde97zRyLYFZCYizPU5T3LwgdYJz1fRhh16bU7u6PPmY7 -p Satoshi

INPUT
    BIP38: 6PYLtMnXvfG3oJde97zRyLYFZCYizPU5T3LwgdYJz1fRhh16bU7u6PPmY7
 Password: Satoshi
  HEX KEY: 0142e026e017d28cb3191c92d12793c7f34b630752dee3847f1b8cfde1291b81ee81ac9990ef7bdaae5b2c

DECRYPTED WIF
      WIF: KwYgW8gcxj1JWJXhPSu4Fqwzfhp5Yfi42mdYmMa4XqK7NJxXUSK7
   LEGACY: 1HmPbwsvG5qJ3KJfxzsZRZWhbm1xBMuS8B
 CASHADDR: bitcoincash:qzm7wt4hmgqcccs7x9ppx2zgjtpg435dzvjhl7zzgd

```

# /bip38
```
go get gitlab.com/acid.sploit/gobip38/bip38
```

This repository contains a Golang BIP38 package exporting all functions needed to encrypt WIF keys, decrypt and generate new BIP38 keys, complying with the BIP38 spec.

- Encrypt WIF key (Compressed & Uncompressed)
- Generate new WIF key and BIP38 encrypt with password
- Generate new intermediary passphrase (EC Multiplied)
- Generate new BIP38 key from intermediary passphrase (EC Multiplied)
- Decrypt BIP38 secret (Non EC Multiplied & EC Multiplied) (Compressed & Uncompressed)


```
// Encrypt WIF with password
func Encrypt(wif *bchutil.WIF, password string) (string, error) 

// Generate intermediate passphrase to generate EC Multiplied keys
func GeneratePassphrase(password string, salt []byte) (string, error)

// Generate EC Multiplied BIP38 key (generate BIP38 key without knowing the private key)
func EncryptECMultiply(intermediatePassphrase string) (string, string, error)

// Catchall Decrypt function - automatically detect ECM or NonECM
func Decrypt(encryptedKey string, password string) (*bchutil.WIF, error)

// Decrypt BIP38 keys where EC Multiplication is not used
func DecryptNonECMultiplied(b []byte, password string) (*bchutil.WIF, error)

// Decrypt BIP38 keys where EC Multiplication is used
func DecryptECMuliplied(b []byte, passphrase string) (*bchutil.WIF, error)

// Return full SHA256(SHA256()) hash
func Hash256(buf []byte) []byte

// Return last 4 bytes of a SHA256(SHA256()) hash
func CheckSum(buf []byte) []byte
```

## Example code

### Decrypt()
```
func decryptBip38(bip38String *string, password *string) (*bchutil.WIF, error) {
	b := base58.Decode(*bip38String)
	wif, err := bip38.Decrypt(*bip38String, *password)
	if err != nil {
		return nil, err
	}

	return wif, nil
}

```

### DecryptNonECMultiplied()
```
func decryptBip38NonECM(bip38String *string, password *string) (*bchutil.WIF, error) {
	byteKey := base58.Decode(*bip38String)
	wif, err := DecryptNonECMultiplied(byteKey, password)
	if err != nil {
		return nil, err
	}

	return wif, nil
}

```

### Encrypt()
```
func encryptWif(wifString *string, password *string) (string, error) {
	wif, err := bchutil.DecodeWIF(*wifString)
	if err != nil {
		return "", err
	}

	encryptWif, err := bip38.Encrypt(wif, *password)
	if err != nil {
		return "", err
	}

	return encryptWif, nil
}
```

### GeneratePassphrase()
```
salt := make([]byte, 4)
rand.Read(salt)

intermediatePassphraseString, _ := bip38.GeneratePassphrase(*passPtr, salt)
```

### EncryptECMultiply()
```
bip38, addr, err := bip38.EncryptECMultiply(intermediatePassphraseString)
if err != nil {
	log.Fatal(err)
	return
}
```